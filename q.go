package q

import (
	"fmt"

	"bitbucket.org/bigwhite/r"
)

func Q() {
	fmt.Println("Q v0.2.0")
	r.R()
}
